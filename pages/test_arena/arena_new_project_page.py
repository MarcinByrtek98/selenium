from selenium.webdriver.common.by import By


class ArenaNewProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_new_project_page_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    # def insert_new_project_details(self, random_title, random_prefix):
    #     self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_title)
    #     self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_prefix)
    #     self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys('Testing 123412341234')

    def insert_random_name(self, random_title):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_title)
    def insert_random_prefix(self, random_prefix):
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_prefix)
    def insert_random_description(self, random_description):
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_description)

    def click_save_a_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()