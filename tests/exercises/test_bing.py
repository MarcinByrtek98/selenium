import time

from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

def test_searching_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://bing.com')
    browser.set_window_size(1920, 1080)
    
    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, '#sb_form_q')
    # Znalezienie guzika wyszukiwania (lupki)
    # search_button = browser.find_element(By.CSS_SELECTOR, '#search_icon')
    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed() is True
    # assert search_button.is_displayed() is True
    # Szukanie 4testers
    search_input.send_keys('4_testers' + Keys.ENTER)
    # Sprawdzenie że pierwszy wynik ma tytuł '4testers'
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, 'h2 a')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Lista seleniumowych webelementów ktore reprezentuja nam tytuły stron
    results = browser.find_elements(By.CSS_SELECTOR, 'h2 a')

    # lista tytułów:
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)

    assert '4_testers – nowy kurs dla testerów oprogramowania' in list_of_titles
    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()
