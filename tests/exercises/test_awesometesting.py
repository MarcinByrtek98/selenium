import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
@pytest.fixture
def browser():
    # 1 - To wykona sie PRZED kazdym testem który korzysta z tego fixture'a
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://awesome-testing.blogspot.com/')
    # 2 - To jest to co zwracamy (przekazujemy) do testów
    yield browser
    # 3 - To wykona sie PO kazdym teście ktory korzysta z tego fixture'a
    browser.quit()


def test_post_count(browser):
    list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    assert len(list_of_titles) == 4


def test_post_count_after_search(browser):
    search_input = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button = browser.find_element(By.CSS_SELECTOR, 'input.gsc-search-button')

    search_input.send_keys('selenium')
    search_button.click()

    wait = WebDriverWait(browser, 10)
    elements_to_wait_for = (By.CSS_SELECTOR, '.status-msg-body')
    wait.until(EC.visibility_of_element_located(elements_to_wait_for))

    list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    assert len(list_of_titles) == 20

def test_post_count_on_2016_label(browser):
    label_element = browser.find_element(By.CSS_SELECTOR, 'a[href="https://awesome-testing.blogspot.com/2016/"]')
    label_element.click()

    list_of_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    assert len(list_of_titles) == 24


