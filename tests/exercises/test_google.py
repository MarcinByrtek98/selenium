import time

from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_google_com():
    browser = Chrome(executable_path=ChromeDriverManager().install())

    browser.get('https://google.com')
    browser.set_window_size(1920, 1080)

    browser.find_element(By.ID, 'L2AGLb').click()

    search_input = browser.find_element(By.CSS_SELECTOR, '#APjFqb')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed() is True

    search_input.send_keys('4_testers')
    search_input.submit()

    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, 'h3')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Lista sleniumowych webelementów ktore reprezentuja nam tytuły stron
    results = browser.find_elements(By.CSS_SELECTOR, 'h3')

    # lista tytułów:
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)

    assert '4_testers – kurs dla testerów oprogramowania #1 w Polsce' in list_of_titles
    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()
