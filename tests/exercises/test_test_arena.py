import time

from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


# Test - uruchomienie Chroma
def test_my_first_chrome_selenium_test():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://demo.testarena.pl/zaloguj')
    # Sprawiamy, żeby przeglądarka byłan a całej stronie
    browser.maximize_window()
    # zaloguj sie
    browser.find_element(By.ID,'email').send_keys('administrator@testarena.pl')
    browser.find_element(By.ID,'password').send_keys('sumXQQ72$L')
    browser.find_element(By.ID,'login').click()

    # asercja - weryfikacja ze logowanie faktycznie się udało
    user_info = browser.find_element(By.CSS_SELECTOR, '.user-info small')
    assert user_info.text == 'administrator@testarena.pl'
    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()


def test_should_open_duckduckgo_com():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://duckduckgo.com')
    assert 'DuckDuckGo' in browser.title
    browser.quit()


# Test - uruchomienie Firefoxa
def test_my_first_firefox_selenium_test():
    # Uruchomienie przeglądarki Firefox. Ścieżka do geckodrivera (drivera dla Firefoxa)
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Firefox(executable_path=GeckoDriverManager().install())

    # Otwarcie strony www.google.pl

    # Weryfikacja tytułu

    # Zamknięcie przeglądarki
    browser.quit()

def test_awesome_testing_blogspot_com():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://awesome-testing.blogspot.com')
    browser.find_element(By.CSS_SELECTOR, 'td .gsc-input').send_keys('selenium')
    browser.find_element(By.CSS_SELECTOR, 'td .gsc-search-button').click()

    results = browser.find_element(By.CSS_SELECTOR, 'div .status-msg-body')
    assert results.text == 'Showing posts sorted by relevance for query selenium. Sort by date Show all posts'
    browser.quit()