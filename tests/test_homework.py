import lorem as lorem
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.test_arena.arena_home import ArenaHomePage
from pages.test_arena.arena_login import ArenaLoginPage
from pages.test_arena.arena_project_page import ArenaProjectPage
from pages.test_arena.arena_new_project_page import ArenaNewProjectPage
from utils.random_message import generate_random_text


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_should_successfully_login(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.verify_displayed_email('administrator@testarena.pl')


def test_should_open_admin_panel(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()

def test_should_open_project_page(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

def test_should_add_a_new_project(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()

    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.click_new_project_page()
    arena_new_project_page = ArenaNewProjectPage(browser)
    arena_new_project_page.verify_new_project_page_title('Dodaj projekt')

    random_title = generate_random_text(10)
    arena_new_project_page = ArenaNewProjectPage(browser)
    arena_new_project_page.insert_random_name(random_title)

    random_prefix = generate_random_text(5)
    arena_new_project_page = ArenaNewProjectPage(browser)
    arena_new_project_page.insert_random_prefix(random_prefix)

    random_description = lorem.sentence()
    arena_new_project_page = ArenaNewProjectPage(browser)
    arena_new_project_page.insert_random_description(random_description)
    arena_new_project_page.click_save_a_new_project()

    arena_project_page.wait_until_new_project_is_added()
    arena_project_page.verify_new_project_has_been_added('Projekt został dodany.')

    arena_project_page.click_projects_section()
    arena_project_page.verify_title('Projekty')

    browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(random_title)

    arena_project_page.check_if_new_project_is_on_the_list()

    arena_project_page.verify_if_project_name_is_on_the_list(random_title)

    browser.quit()


